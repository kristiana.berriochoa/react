import React from 'react';
import Item from './Item';

class List extends React.Component {
	
	render() {
		return (
			<div>
				{this.props.categories.map(function(category, i){
					return (
						<div className = 'list' key = {i}>
							<Item category = {category}/>
						</div>
					)
				})}
			</div>
		)
	}
}

export default List;