import React from 'react';
import List from './List';

class Main extends React.Component {
  
  render() {

    var categories = ["bushes", "grasslands", "flowers", "trees", "weeds"]

    return (
      <div>
        <h1>Flora (Plants):</h1>
        <List categories = {categories}/>
      </div>
    )
  }
}

export default Main;