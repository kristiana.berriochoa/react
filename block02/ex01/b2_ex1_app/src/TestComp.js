import React from 'react'
import TestTwoComp from './Child'

export default class TestComp extends React.Component {
    render(){
        return(
            <div>
                <h1>I am TestComp!</h1>
                <TestTwoComp/>
            </div> 
        )
    }
}
