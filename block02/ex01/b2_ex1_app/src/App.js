import React from 'react';
import Child from './Child'
//import logo from './logo.svg';
import './App.css';

class App extends React.Component {
  
  render() {
    let name = "Kristiana"
    return (
      <div className = "phrase">
        <Child name = {name}/>
      </div>  
    );
  }
}

export default App;