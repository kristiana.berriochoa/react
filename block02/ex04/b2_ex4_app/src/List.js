import React from 'react';
import Item from './Item';

function List(props) {

	return (
		<div>
			{props.categories.map(function(category, i){
			    return (
				    <div className = 'list' key = {i}>
					    <Item category = {category}/>
				    </div>
				)
			})}
		</div>
	)
}

export default List;