import React from 'react';
import List from './List';

function Main() {

  var categories = ["bushes", "grasslands", "flowers", "trees", "weeds"];

  return (
    <div>
      <h1>Flora (Plants):</h1>
      <List categories = {categories}/>
    </div>
  )
}

export default Main;