## Exercise 2:

Create a parent component inside which you define two arrays of equal length, the first will have 5 firstnames and the second 5 lastnames.

Create two children components – one to display the firstnames and one to display the lastnames so that they are rendered side by side with matching order.