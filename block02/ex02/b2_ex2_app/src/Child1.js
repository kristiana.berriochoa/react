import React from 'react';

class Child1 extends React.Component {
  
  render() {
    return (
      <h2>
        {this.props.lastname}
      </h2>
    );
  }
}

export default Child1;