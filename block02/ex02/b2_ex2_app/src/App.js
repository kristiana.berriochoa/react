import React from 'react';
import Child from './Child';
import Child1 from './Child1';
//import logo from './logo.svg';
import './App.css';

class App extends React.Component {
  
  render() {
    
    var names = ["Jane", "Mary", "Charlotte", "Agatha", "Sylvia"];
    var lastnames = ["Austen", "Shelley", "Bronte", "Christie", "Plath"]

    var looper = names.map(function(name, i){
      return (
        <div className = "kids" key = {i}>
            <Child name = {name}/>
            <Child1 lastname = {lastnames[i]}/>
        </div>
      )
    });
    return (
      <div>
        <h1>Great Authors:</h1>
        {looper}
      </div>
    )
  }
}

export default App;