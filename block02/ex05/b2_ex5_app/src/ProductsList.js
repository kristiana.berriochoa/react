import React from 'react';
import SingleProduct from './SingleProduct';

function ProductsList(props) {

	let sale;
    const allProds = props.products.map(function(product, i) {
        if(product.onSale === true){
            sale = "On Sale!"
        } else {
            sale = null;
        }
        return (
            <div key = {i}>
				<SingleProduct product = {product} sale = {sale}/>
            </div>
        )
	});
	return (
        <div className = 'main-container'>
            {allProds}
        </div>
    )
}

export default ProductsList;

/*
function ProductsList(props) {
	
	return (
		<div className = "main-container">
			{props.products.map(function(product, i){
				return (
					<div key = {i}>
						<SingleProduct product = {product}/>
					</div>
				)
			})}
		</div>
	)	
}

export default ProductsList;
*/