import React from 'react';

function Header() {

    return (
        <div>
            <header>
                <ul className = 'topnav'>
                    <li>Home</li>
                    <li>Shop</li>
                    <li>Customize</li>
                    <li>Contact</li>
                </ul>
                <h1 className = "title">Shop Til You Drop!</h1>
            </header>
        </div>
    )
}

export default Header;