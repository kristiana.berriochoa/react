import React from 'react';

function SingleProduct(props) {

	return (
	    <div className = "product">
            <img src = {props.product.image} alt = "item"/>
			<h3>{props.product.product}</h3>
			<h4 className = "price">Price: ${props.product.price.toFixed(2)}</h4>
			<h2><span className = "sale">{props.sale}</span></h2>
		</div>
	)
}

export default SingleProduct;