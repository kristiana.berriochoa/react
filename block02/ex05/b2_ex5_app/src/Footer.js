import React from 'react';
import './App.css';

function Footer() {

  return (
    <footer className = "footer">
      <h4>Contact Us</h4>
      <p>Call: +34.934.811.11</p>
      <p>Email: order@shoptilyoudrop.org</p>
      <p className = 'copyright'>© 2020 Copyright | Shop Til You Drop!</p>
    </footer>
  );
}

export default Footer;