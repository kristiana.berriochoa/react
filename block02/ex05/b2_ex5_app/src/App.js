import React from 'react';
import ProductsList from './ProductsList';
import Header from './Header'
import Footer from './Footer'
//import logo from './logo.svg';
import './App.css';

function App() {

  var products = [
    {
      product    : 'flash t-shirt',
      price      :  27.50,
      category   : 't-shirts',
      bestSeller :  false,
      image      : 'https://www.juniba.pk/wp-content/uploads/2018/02/the-flash-distressed-logo-t-shirt-black.png',
      onSale     :  true
    },
    {
      product    : 'batman t-shirt',
      price      :  22.50,
      category   : 't-shirts',
      bestSeller :  true,
      image      : 'https://s1.thcdn.com/productimg/1600/1600/11676326-1444552242012324.png',
      onSale     :  false
    },
    {
      product    : 'superman hat',
      price      :  13.90,
      category   : 'hats',
      bestSeller :  true,
      image      : 'https://banner2.kisspng.com/20180429/rqe/kisspng-baseball-cap-superman-logo-batman-hat-5ae5ef317f8366.9727520615250184175223.jpg',
      onSale     :  false
    }
  ];

  return (
    <div className = "wrapper">
      <Header/>
      <div>
        <h2 className = 'section-title'>Our Products</h2>
        <ProductsList products = {products}/>
      </div>
      <Footer/>
    </div>
  )
}

export default App;