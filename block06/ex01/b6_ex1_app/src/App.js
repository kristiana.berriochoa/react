import React from 'react';
import Navbar from './Navbar';
import Home from './Home';
import About from './About';
import Gallery from './Gallery';
import Contact from './Contact';
import './App.css';

class App extends React.Component {

  state = {
    currentPage: "home"
  }

  changePage = (currentPage) => {
    this.setState({ currentPage: currentPage.toLowerCase() });
  }

  render() {
    
    let { currentPage} = this.state;
    let current;

    if(currentPage === "home"){
      current = <Home/>
    } if(currentPage === "about"){
      current = <About/>
    } if(currentPage === "gallery"){
      current = <Gallery/>
    } if(currentPage === "contact us"){
      current = <Contact/>
    }

    return (
      <div>
        <Navbar changePage = {this.changePage}/>
        <div style = {style}>{current}</div>
      </div>
    )
  }
}

export default App;

const style = {
  margin: "30px",
  color: "maroon"
}