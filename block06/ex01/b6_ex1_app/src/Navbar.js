import React from 'react'

class Navbar extends React.Component {
    
    render() {
        return (
            <ul style = {navStyle} onClick = {(e) => this.props.changePage(e.target.textContent)}>
                <li>Home</li>
                <li>About</li>
                <li>Gallery</li>
                <li>Contact Us</li>
            </ul>
        )
    }
}

export default Navbar;

let navStyle = {
    display: "flex",
    justifyContent: "space-around",
    listStyle: "none",
    color: "white",
    fontWeight: "bold",
    backgroundColor: "black",
    padding: 0,
    margin: 0,
    height: "40px",
    alignItems: "center",
}