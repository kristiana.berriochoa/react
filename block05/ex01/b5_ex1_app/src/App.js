import React from 'react';
import Item from './Item';
import './App.css';

class App extends React.Component {

  state = {
    items: [
      {
        title: "The Great Gatsby",
        category: "Drama",
        image: 'https://images-na.ssl-images-amazon.com/images/I/41iers%2BHLSL._SX326_BO1,204,203,200_.jpg',
        price: 21.95,
        quantity: 1,
      },
      { 
        title: "Frankenstein",
        category: "Horror",
        image: 'https://images-na.ssl-images-amazon.com/images/I/416QjTxduzL._SX311_BO1,204,203,200_.jpg',
        price: 19.95,
        quantity: 1,
      },
      {
        title: "Little Women",
        category: "Drama",
        image: 'https://images-na.ssl-images-amazon.com/images/I/51BwNaRY1SL.jpg',
        price: 17.65,
        quantity: 1,
      }
    ],
    category: " ",
    filtered: [],  
  }

  handleChange = (e) => {this.setState({ category: e.target.value })}

  handleClick = (e) => {
    e.preventDefault();
    var newList = this.state.items;
    var filteredItems = newList.filter(item => item.category.toLowerCase() === this.state.category.toLowerCase())
      this.setState({ filtered: filteredItems });
  }

  render() {
    
    let display;

    if(this.state.category === " "){
      display = this.state.items.map((item, i) => <Item key = {i} item = {item}/>) 
    }
    if(this.state.category !== " "){
      if(this.state.category === "all"){
        display = this.state.items.map((item, i) => <Item key = {i} item = {item}/>) 
      } else {
        display = this.state.filtered.map((item, i) => <Item key = {i} item = {item}/>)
      }
      if(this.state.category === "scifi" || this.state.category === "comedy" || this.state.category === "foreign" ){
        display = <h2>No category found!</h2>
      }
    } 

    return (
      <div className = "shopCart">
        <h1>Books, Books, Books!</h1>
        <h2>Please search by category below:</h2>
        <form>
            <input onChange = {this.handleChange} type = "text" placeholder = "Search Category"></input>
            <button onClick = {this.handleClick}>Search</button>
        </form>
        <div className = "display">{display}</div>
      </div>
    )
  }
}

export default App;

/*
handleClick = (itemFilter) => {
  const results = this.state.items.filter(item =>  
    item.toLowerCase().includes(this.state.category)
  ); 
  this.setState({searchResults: results});
}

handleClick = (category) => {
  let currentList = [];
  let newList = [];
  if(category !== " "){
    currentList = this.props.items;
    newList = currentList.filter(item => {
      const lc = item.toLowerCase();
      const filter = category.toLowerCase();
      return lc.includes(filter);
    });
  } else {
    newList = this.props.items;
  }
  this.setState({filtered: newList})
  this.handlClick = this.handleClick.bind(this)
}
*/