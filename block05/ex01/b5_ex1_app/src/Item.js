import React from 'react';

class Item extends React.Component {

    render() {
        return (
            <div className = "items"> 
                <div className = "item">
                    <h3>{this.props.item.title}</h3>
                    <img src = {this.props.item.image} alt = "bookcover"/>
                    <div className = "item-extra">
                        <div className = "price">Price: ${this.props.item.price}</div>
                        <div className = "quantity">Quantity: {this.props.item.quantity}</div>
                        <div className = "category">Category: {this.props.item.category}</div>
                    </div>
                </div>
            </div>
        )
    }
}  
 
export default Item;

/* 
class Item extends React.Component {

    render() {
        return (
            <div className = "items"> {this.props.items.map((item, i) => {
                return <div className = "item" key = {i}>
                <h3>{item.title}</h3>
                    <img src = {item.image} alt = "bookcover"/>
                    <div className = "price">Price: ${item.price}</div>
                    <div className = "quantity">Quantity: {item.quantity}</div>
                    <div className = "category">Category: {item.category}</div>
                </div>})
            }</div>
        )
    }
} 

export default Item;
*/
