import React from 'react';

class Message extends React.Component {

    sendMessage = (props) => {

        var message = " ";

        var item = "@";
        var emailOK = false;
        if(props.email.includes(item)){
            emailOK = true;
        }
        
        if(this.props.registered === true){
            if(emailOK === false){
                message = <h3>Please, provide a valid email.</h3>
            } else if (props.password.length < 8){
                message = <h3>Password must have at least 8 characters.</h3> 
            } else if (props.password !== props.confPassword){
                message = <h3>Passwords should match.</h3>
            } else {
                message = <h3>Successfully registered!</h3>
            }
        }
    }

    render() {
        return (
           <div>{message}</div>
        )
    }
}

export default Message;