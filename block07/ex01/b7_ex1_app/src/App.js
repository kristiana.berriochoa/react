import React from 'react';
//import Message from './Message'
import './App.css';

class App extends React.Component {

  state = {
    email: " ",
    password: " ",
    confPassword: " ",
    registered: false,
  }

  handleEmailChange = (e) => {this.setState({ email: e.target.value })}

  handlePasswordChange = (e) => {this.setState({ password: e.target.value })}

  handlePasswordConfChange = (e) => {this.setState({ confPassword: e.target.value })}

  //handleChange = (e) => {this.setState({ [e.target.name]: e.target.value })}

  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({ registered: true });
    setTimeout(() => this.setState({ registered: false }), 2000) 
  }

  render() {

    let { email, password, confPassword, registered } = this.state
    var message = " ";

    var item = "@";
    var emailOK = false;
    if(email.includes(item)) {
      emailOK = true;
    }
    
    if(registered === true){
      if(emailOK === false){
        message = <h3 style = {red}>Please, provide a valid email.</h3>
      } else if(password.length < 8){
        message = <h3 style = {red}>Password must have at least 8 characters.</h3> 
      } else if(password !== confPassword) {
        message = <h3 style = {red}>Passwords should match.</h3>
      } else {
        message = <h3 style = {green}>Successfully registered!</h3>
      }
    }

    return (
      <div className = "email-app">
        <h1>Email - Sign In</h1>
        <h2>Please, fill out the form below to register:</h2>
        <form onSubmit = {this.handleSubmit}>
          <input onChange = {this.handleEmailChange} type = "email" placeholder = "Email"/>
          <input onChange = {this.handlePasswordChange} type = "password" placeholder = "Password"/>
          <input onChange = {this.handlePasswordConfChange} type = "password" placeholder = "Confirm Password"/>
          <button>Register</button>
        </form>
        <div className = "message">{message}</div>
      </div>
    )
  }
}

const red = {
  backgroundColor: "red",
  color: "white",
  fontWeight: "bold",
  textAlign: "center",
  padding: "10px",
}

const green = {
  backgroundColor: "green",
  color: "white",
  fontWeight: "bold",
  textAlign: "center",
  padding: "10px",
}

export default App;