import React from 'react';

const Button = (props) => {

	const handleClick = (e) => {
		e.preventDefault()
	    alert(props.accountInfo());
    };
	
	return (
	  <div>
		<button onClick = {handleClick}>Submit</button>  
	  </div>
	);
}
  
export default Button;

/*class Button extends React.Component {

    handleOnClick = () => {
		alert(this.props.accountInfo());
    };

	render() {
		return (
			<div>
				<button onClick = {this.handleOnClick}>Submit</button>
			</div>
		)
	}
}

export default Button;*/