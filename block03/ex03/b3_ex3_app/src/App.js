import React, { useState } from 'react';
import Email from './Email';
import Password from './Password';
import Button from './Button';
import './App.css';

const App = () => {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const accountInfo = () => {
		return `Email: ${email}; Password: ${password}`
	};

  return (
    <div>
      <h3>Sign In:</h3>
      <Email onChange = {value => setEmail(value)} email = {email}/> 
      <Password onChange = {value => setPassword(value)} password = {password}/>
      <Button accountInfo = {accountInfo}/> 
    </div>
  );
}

export default App;

/*class App extends React.Component {
  
  state = { };
  
  setEmail = (email) => {this.setState({ email })}

  setPassword = password => {this.setState({ password })}

  const accountInfo = () => {"Email: " + this.state.email + ";" + " Password: " + this.state.password};

  render() {
    return (
      <div>
        <Email getEmail = {this.getEmail} />
          <h1>{this.state.email}</h1>
        <Password getPassword = {this.getPassword} />
          <h1>{this.state.password}</h1>
        <Button accountInfo = {this.accountInfo} />
      </div>
    )
  }
}

export default App;*/