import React from 'react';

const Password = (props) => {
  
    const handleChange = (e) => props.onChange(e.target.value);
    
    return (
      <div className = "password">
        <input onChange = {handleChange} type = "password" placeholder = "Password"/>
      </div>
    );
}
  
export default Password;

/*class Password extends React.Component {

  state = {
    password: " "
  }

  handleChange = (e) => {
	  this.setState({password: e.target.value})
	  this.props.setPassword(e.target.value);
  }

  render() {
	  return (
		  <div>
			  <input onChange = {this.handleChange} type = "password" placecard = "Password" value = {this.state.password}/>
	    </div>
	  )
  }
}

export default Password;*/