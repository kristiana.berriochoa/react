import React from 'react';

const Email = (props) => {
  
	const handleChange = (e) => props.onChange(e.target.value);
	
	return (
	  <div>
		<input onChange = {handleChange} type = "email" placeholder = "Email"/>
	  </div>
	);
}
  
export default Email;

/*class Email extends React.Component {
    
    state = {
        email: " "
    }

	handleChange = (e) => {
		this.setState({email: e.target.value})
		this.props.setEmail(e.target.value);
	}

	render() {
		return (
			<div>
				<input onChange = {this.handleChange} type = "email" placecard = "Email" value ={this.state.email}/>
	        </div>
	    )
    }
}

export default Email;*/