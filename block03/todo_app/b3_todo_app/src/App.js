import React, { useState } from 'react';
//import Form from "./Form"
import './App.css';

function App() {

  const[todos, setTodos] = useState(
    [
      {text: "Study for euskara exam", done: false},
      {text: "Build ToDo app", done: false},
      {text: "Get haircut", done: false}
    ]
  );

  const[value, setValue] = useState(" ");

  const handleChange = (e) => setValue(e.target.value);
  
  const add = (text) => {
    var newTodos = [...todos];
    newTodos.push({ text })
    setTodos(newTodos)
  };
  
  const handleSubmit = (e) => {
    e.preventDefault();
    add(value);
    setValue(" ");
  };

  const completed = (i) => {
    var newTodos = [...todos];
    newTodos[i].done = true;
    setTodos(newTodos)
  };

  const removed = (i) => {
    var newTodos = [...todos];
    newTodos.splice(i, 1);
    setTodos(newTodos);
  }; 

  return (
    <div className = "app">
      <div className = "list">
        <h2>My Tasks</h2>
        <div>{todos.map((todo, i) => (
          <li className = "todo" key = {i} style = {{textDecoration: todo.done ? "line-through" : null }}>{todo.text} 
            <div className = "complete"><button onClick = {() => completed(i)}>√</button></div>
            <div className = "remove"><button onClick = {() => removed(i)} >X</button></div>
          </li>
        ))}
        </div>
        <form onSubmit = {handleSubmit}>
          <input onChange = {handleChange} value = {value}/>
          <button>Submit</button>
        </form>
      </div>
   </div>
  );
}

export default App;


/*
const Todo = ({ todo, i }) => 
  <div className = "todo">
    {todo.text}
    <button onClick = {() => completed(i)}>Done</button>
    <button onClick = {() => removed(i)}>X</button>
  </div>
*/