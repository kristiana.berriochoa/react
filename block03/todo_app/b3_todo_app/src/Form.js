import React, { useState } from 'react';

function Form( { add }) {
  const[value, setValue] = useState(" ");

  const handleChange = e => {
    setValue(e.target.value)
  }

  const handleSubmit = e => {
    e.preventDefault();
    if(!value) return;
    add(value);
    setValue(" ");
  }
  return (
    <div>
      <form onSubmit = {handleSubmit}>
        <input onChange = {handleChange} value = {value}/>
      </form>
    </div>
    
  );
}

export default Form;