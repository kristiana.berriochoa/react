import React, { useState } from 'react';
//import logo from './logo.svg';
import './App.css';

const App = () => {

  const [text, setText] = useState('');

  const handleChange = (e) => setText(e.target.value);
  
  return (
    <div className = 'user-text'>
      <input onChange = {handleChange}/> 
      <h1>{text}</h1> 
      {text === '' ? <h1>{"No data provided!"}</h1> : null} 
    </div>
  );
}

export default App;