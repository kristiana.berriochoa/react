## Exercise 2 / Optional with hooks:

Refactor the previous exercise, this time using a functional component and the `useState` hook. Take a look at `useState` in the "Hooks" block of React part. 