import React from 'react';
//import logo from './logo.svg';
import './App.css';

class App extends React.Component {
  
  state = {
    text: '',
  }
  
  handleChange = (e) => {
    this.setState({[e.target.name]: e.target.value})
  }

  render() {
    return (
      <div className = 'user-text'>
        <input onChange = {this.handleChange} name = "text" value = {this.state.text}/>
        <h1>{this.state.text}</h1>
        {this.state.text === '' ? <h1>{"No data provided!"}</h1> : null}
      </div>
    )
  }
}

export default App;

/*
class App extends React.Component {
  
  state = {
    text: " ",
    done: false
  }
  
  handleChange = (e) => {
    this.setState({[e.target.name]: e.target.value})
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({done: true})
  }

  render() {
    let {text, done} = this.state;
    return (
      <div>
        <form className = 'form' onSubmit = {this.handleSubmit}>
          <input onChange = {this.handleChange} name = "text" value = {text}/>
          <button onSubmit = {this.handleSubmit}>Submit</button>
        </form>
        {done ? <h1>{text}</h1> : <h1>{"No data provided!"}</h1>}
      </div>
    )
  }
}

export default App;
*/