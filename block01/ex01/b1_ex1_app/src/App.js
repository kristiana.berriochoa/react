import React from 'react';
//import logo from './logo.svg';
import './App.css';

const App = () => {

  var categories = ['t-shirts', 'hats', 'shorts', 'shirts', 'pants', 'shoes']

  const renderCategories = categories.map((category, i) => {
    return <h2 key = {i}>{category}</h2>
  })

  return <div>
    <h1>Available Products:</h1>
    {renderCategories}
  </div>
}

export default App;

/*
class App extends React.Component {
  render() {
    var categories = ['t-shirts', 'hats', 'shorts', 'shirts', 'pants', 'shoes']
    return (
      <div>
        <h1>Available Products:</h1>
        {categories.map((category, i) => <h2 key = {i}>{category}</h2>)}
      </div>
    );
  }
}

export default App;
*/