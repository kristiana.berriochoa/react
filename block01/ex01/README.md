## Exercise 1:

Create a brand new React application with 'React create App'. This app needs to display a list of product categories from an array. 

The categories are 't-shirts', 'hats', 'shorts', 'shirts', 'pants', 'shoes'. In order to display them you will need to use Array.map.

***Your solution goes to the ex01 folder***