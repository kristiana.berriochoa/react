import React from 'react';
import Header from './Header';
import Main from './Main';
import BestSellers from './Bestsellers'
import Footer from './Footer'
//import logo from './logo.svg';
import './App.css';

class App extends React.Component {
  
  render() {
    return (
      <div className = 'wrapper'>
        <div><Header/></div>
        <div>
          <h2 className = 'section-title'>Our Products</h2>
          <Main/>
        </div>
        <div>
          <h2 className = 'section-title'>Our Bestsellers</h2>
          <BestSellers/>
        </div>
        <div><Footer/></div>
      </div>
    );
  }
}

export default App;