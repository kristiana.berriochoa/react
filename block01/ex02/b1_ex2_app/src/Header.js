import React from 'react'
import './App.css';

class Header extends React.Component {
	
	render() {
		return(
			<div>
				<header>
					<ul className = 'topnav'>
						<li>Home</li>
						<li>Store</li>
						<li>Customize</li>
						<li>My Account</li>
					</ul>
					<h1 className = 'title'>Shop Til You Drop!</h1>
				</header>
			</div>
		);
	}
}

export default Header;