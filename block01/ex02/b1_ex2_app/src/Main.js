import React from 'react';
import Products from './Products'

function Main() {
    
    let sale;
    const allProds = Products.map(function(product, i) {
        if(product.onSale === true){
            sale = "On Sale!"
        } else {
            sale = null;
        }
        return (
            <div className = 'main' key = {i}>
                <div className = 'product'>
                    <img src = {product.image} alt = "item"/>
                    <h3>{product.product}</h3>
                    <h4 className = 'price'>Price: ${product.price.toFixed(2)}</h4>
                    <h2><span className = "sale">{sale}</span></h2>
                </div>
            </div>
        )
    });
    return (
        <div className = 'main-container'>
            {allProds}
        </div>
    )
}

export default Main;