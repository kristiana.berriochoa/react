import React from 'react';
import Products from './Products'

function BestSellers() {
    
    var Best = Products.map(function(product, i) {
        if(product.bestSeller === true){
            return (
                <div className = "bestsells" key = {i}>
                    <div className = 'product'>
                        <img src = {product.image} alt = "item"/>
                        <h3>{product.product}</h3>
                        <h4 className = 'price'>Price: ${product.price.toFixed(2)}</h4>
                    </div> 
                </div>
            )
        }
    });
    return (
        <div className = "main-container">
            {Best}
        </div>
    )
}

export default BestSellers;