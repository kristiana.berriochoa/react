import React from 'react';

class Counter extends React.Component {

  state = {
    count: 0,
    clicks: 1,
  }

  handleOnClick = () => {
    let { count, clicks } = this.state;
    this.setState({ clicks: clicks + 1 });
    if(this.state.clicks % 2 !== 0) {
      this.setState({ count: count + 1 });
    }
  };

  render() {
    return (
      <div>
        <div><h1>{this.state.count}</h1></div>
        <button onClick = {this.handleOnClick}>Click Me</button>
      </div>
    );
  }
}

export default Counter;