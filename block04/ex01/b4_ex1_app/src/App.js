import React, { useState } from 'react';
//import Counter from './Counter'
import './App.css';

const Counter = () => {

  const [count, setCount] = useState(0);
  const [clicks, setClicks] = useState(1);

  function handleClick() {
    setClicks(clicks + 1);
    if(clicks % 2 !== 0) {
      setCount(count + 1);
    }
  };

  return (
    <div>
      <h1>Counter:</h1>
      <button onClick = {handleClick}>Click</button>
      <h1>{count}</h1>
    </div>
  );
}

export default Counter;


/*class App extends React.Component {

  state = { }

  getData = (data) => { this.setState({ data }) }

  render() {
    return (
      <div>
        <h1>Counter:</h1>
        <Counter getData = {this.getData}/>
        <h1>{this.state.data}</h1>
      </div>
    );
  }
}

export default App;*/