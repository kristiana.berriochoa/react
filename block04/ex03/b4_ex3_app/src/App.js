import React from 'react';
import './App.css';

class App extends React.Component {

  state = {
    email: " ",
    password: " ",
    correctEmail: false,
    correctPassword: false,
    message: " ",
    isGreen: true,
    submitted: false,
    timer: undefined,
    users: [
      {
        email: "kjb@gmail.com",
        password: "1234"
      },
      {
        email: "idaho@gmail.com",
        password: "5678"
      },
    ],
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      this.setState ({ timer : this.state.timer + 1 })
    }, 1000) 
  }; 

  componentWillUnmount(){
    clearInterval(this.interval)
  }

  handleEmailChange = (e) => {this.setState({ email: e.target.value })}

  handlePasswordChange = (e) => {this.setState({ password: e.target.value })}

  handleClick = (e) => {
    e.preventDefault();
    let timer = 0;
    this.state.users.map(user => {
      if(user.email === this.state.email && user.password === this.state.password){
        return this.setState({
          submitted: true,
          isGreen: true,
          timer,
          message: "Congrats, you've entered the correct information!"
        })
      } else {
        return this.setState({
          submitted: true,
          isGreen: false,
          timer,
          message: "Sorry, you've entered incorrect information."
        })
      }
    });
  }

  render() {
    return (
      <div className = "email">
        <h1>Welcome, You've Got Mail!</h1>
        <form>
          <input onChange = {this.handleEmailChange} placeholder = "Email" type = "email"/>
          <input onChange = {this.handlePasswordChange} placeholder = "Password" type = "password"/>
          <button onClick = {this.handleClick}>Submit</button>
        </form>
        <div>{this.state.submitted === true ? 
          <div style = {this.state.isGreen === true ? style.green : style.red}>
            {this.state.timer === 1 ? <h2>{this.state.message} This message will disappear in 3.</h2> :
            this.state.timer === 2 ? <h2>{this.state.message} This message will disappear in 2.</h2> :
            this.state.timer === 3 ? <h2>{this.state.message} This message will disappear in 1.</h2> :
            this.state.timer === 4 ? <h2>{this.state.message} This message will disappear in 0.</h2> :
            this.state.timer > 4 ? null : null}
          </div> 
          : null 
        }</div>
      </div>
    );
  }
}

export default App;

const style = {
  green: {
    color: 'green'
  },
  red: {
    color: 'red'
  }
}