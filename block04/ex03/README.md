## Exercise 3:

Create a form that gets email and password and a component with an alert initially not visible. When submit it should check in an array of users if the email exists and if so, if the corresponding password is correct. 

If email and password are correct you should display a positive message with a green background otherwise negative message and red background but in both cases a series of messages:

    This message will disappear in 3
    This message will disappear in 2
    This message will disappear in 1
    This message will disappear in 0

and make the alert component invisible again i.e. unmount it.

> Use `setInterval` to display every message for 1 second

***Your solution goes to the ex03 folder***