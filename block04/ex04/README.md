## Exercise 4:

Create a cart component that will display a list of products in cart. For the products you can use the same array from the products page but with a new key value pair for the quantity.

Requirements:

- you can use React Hooks or class components for this exercise
- you should display the total cost of the products
- you should be able to change the product's quantity
- you should display a free shipping green message once the total gets to 500€ or more, otherwise not eligible for a free shipping red message.

***Your solution goes to the ex04 folder***