import React from 'react'; 

class Total extends React.Component {

  render() {
    
    var total = 0;

    return (
      <div>{this.props.items.map((item, i) => {
        total += this.props.items[i].price * this.props.items[i].quantity
        return null;
        })
      }
        <h2 className = "total">Cart Total: ${total.toFixed(2)}</h2>
        {total > 500 ? <div className = "green">* You have spent more than $500 and qualify for free shipping!</div> : 
        <div className = "red">* In order to receive free shipping you must spend more than $500.</div>}
      </div>
    )
  } 
}

export default Total;


/*//let message;

return (
  <div> { 
    this.props.items.map((item, i) => {
      total += this.props.items[i].price * this.props.items[i].quantity
      if(total > 500) {
        message = "* You have spent more than $500 and qualified for free shipping!";
      } else {
        message = "* In order to receive free shipping you must spend more than $500.";
      }
    })
  }
    <h2>Cart Total: ${total.toFixed(2)}</h2> 
    <div className = "red">{message}</div>
  </div>
)
} 
}*/