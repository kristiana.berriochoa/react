import React from 'react';
import Total from './Total'; 
//import Item from './Item'
import './App.css';

class App extends React.Component {

  state = {
    items: [
      {
        title: 'The Great Gatsby',
        image: 'https://images-na.ssl-images-amazon.com/images/I/41iers%2BHLSL._SX326_BO1,204,203,200_.jpg',
        price: 21.95,
        quantity: 1,
      },
      { 
        title: 'Frankenstein',
        image: 'https://images-na.ssl-images-amazon.com/images/I/416QjTxduzL._SX311_BO1,204,203,200_.jpg',
        price: 19.95,
        quantity: 1,
      },
      {
        title: 'Little Women',
        image: 'https://images-na.ssl-images-amazon.com/images/I/51BwNaRY1SL.jpg',
        price: 17.65,
        quantity: 1,
      }
    ]
  } 

  handleChange = (e, i) => {
    let inputValue = e.target.value;
    this.setState(prevState => {
      let items = [...prevState.items];
      items[i] = {...items[i], quantity: inputValue};
      return { items };
    });
  }

  render() {
    return (
      <div className = "shopCart">
        <h1>Let's Shop!</h1>
        <h2>Thanks for shopping with us, but it's time to checkout. Please review your order below:</h2>
          <div className = "items"> {this.state.items.map((item, i) => {
            return <div className = "item" key = {i}>
              <h3>{item.title}</h3>
              <img src = {item.image} alt = "bookcover"/>
              <div className = "price">Price: ${item.price}</div>
              <div className = "quantity">Quantity:</div>
              <input onChange = {e => this.handleChange(e, i)} value = {item.quantity}  type = "number"/>
            </div>;  
            })
          } </div>
        <Total items = {this.state.items}/>
      </div>
    )
  }
}

export default App;

/*OnIncrease = () => {
  let counter = this.state.counter + 1;
  this.setState({counter});
 }

 OnDecrease = () => {
   if(this.state.counter > 0){
     let counter = this.state.counter - 1;
     this.setState({counter});
   } 
 }
 <p>Quantity: {item.quantity}</p>
 <button onClick = {this.OnIncrease}>+</button>
 <button onClick = {this.OnDecrease}>-</button>
 
  onChangeQuantity = (e, i) => {
    var items = this.state.items;
    var value = e.target.value;
    var valueInt = value;

    if(value === " ") {
      items[i].quantity = value;
    } else if (valueInt > 0 && valueInt < 100) {
      items[i].quantity = valueInt;
    }
    this.setState({items});
  }*/