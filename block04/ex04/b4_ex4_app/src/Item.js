import React from 'react'; 

class Item extends React.Component {
    
    state = {
        quantity: this.props.quantity
    };

    increment(){
        this.setState({
            quantity: this.state.quantity + 1
        });
    }

    decrement(){
        this.setState({
            quantity: this.state.quantity - 1
        });
    }

    render() {
        return (
            <div>
                {this.props.title}, {this.state.quantity}
                <button onClick = {this.increment}>+</button>
                <button onClick = {this.decrement}>-</button>
            </div>
        )
    }
}

export default Item;