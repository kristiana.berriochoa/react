import React from 'react';

class Euro extends React.Component {
    
    render() {
        return (
            <div>
                <h2>{`$${this.props.convertEUR}`}</h2>
            </div>
        )
    } 
}

export default Euro;

/*const Euro = (props) => {

    const handleEuroChange = (e) => props.onChange(e.target.value);
    
        return (
            <div>
                <input onChange = {handleEuroChange} placeholder = "EUR to USD"/>
                <h2>=</h2>
                <h2>{`${props.convertEUR}`}</h2>
            </div>
        )
}

export default Euro;
*/