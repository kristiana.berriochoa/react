import React from 'react';

class Dollar extends React.Component {
    
    render() {
        return (
            <div>
                <h2>{`${this.props.convertUSD}€`}</h2>
            </div>
        )
    } 
}

export default Dollar;


/*const Dollar = (props) => {

    const handleDollarChange = (e) => props.onChange(e.target.value);
    
        return (
            <div>
                <input onChange = {handleDollarChange} placeholder = "USD to EUR"/>
                <h2>=</h2>
                <h2>{`${props.convertUSD}€`}</h2>
            </div>
        )
}

export default Dollar;*/