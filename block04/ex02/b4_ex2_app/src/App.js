import React from 'react';
import Euro from './Euro';
import Dollar from './Dollar';
import './App.css';

class App extends React.Component {

  state = {
    euro: 0,
    dollar: 0,
    exRate: 1,
  }

  componentDidMount(){
    fetch('http://www.apilayer.net/api/live?access_key=f70c40807abaff3e6d9ceaf189910bdc')
      .then(res => res.json())
      .then(data => 
        this.setState({ exRate: data.quotes.USDEUR })
      )
    .catch(error => console.log(error))
  }

  handleEuroChange = (e) => {
    this.setState({euro: e.target.value})
  }

  handleDollarChange = (e) => {
    this.setState({dollar: e.target.value})
  }

  convertEUR = () => {
    return (this.state.euro / this.state.exRate).toFixed(2)
  }

  convertUSD = () => {
    return (this.state.dollar * this.state.exRate).toFixed(2)
  }

  render () {
    return (
      <div className = "converter">
        <h1>Let's Make Conversions!</h1>
        <div className = "conversions">
          <div className = "singleConversion">
            <input onChange = {this.handleEuroChange} placeholder = "EUR to USD"/>
            <h2>=</h2>
            <Euro convertEUR = {this.convertEUR()}/>
          </div>
          <div className = "singleConversion">
            <input onChange = {this.handleDollarChange} placeholder = "USD to EUR"/>
            <h2>=</h2>
            <Dollar convertUSD = {this.convertUSD()}/>
          </div>
        </div>
      </div>
    )
  }
} 

export default App;

/*
import React, { useState, useEffect } from 'react';
import Euro from './Euro';
import Dollar from './Dollar';
import './App.css';

function App() {

  const [euro, setEuro] = useState(0);
  const [dollar, setDollar] = useState(0);
  const [exRate, setRate] = useState(1);

  useEffect(() => {
    fetch('http://www.apilayer.net/api/live?access_key=f70c40807abaff3e6d9ceaf189910bdc')
    .then(res => res.json())
    .then(data => 
      setRate({ exRate: data.quotes.USDEUR })
    )
    .catch(error => console.log(error))
  }, [])

  const convertEUR = () => {
    return (euro / exRate).toFixed(4)
  }

  const convertUSD = () => {
    return (dollar * exRate).toFixed(4)
  }

  return (
    <div className = "App">
      <h1>Let's Do Conversions!</h1>
      <div className = "converter">
        <Euro onChange = {value => setEuro(value)} euro = {euro} convertEUR = {convertEUR()}/>
        <Dollar onChange = {value => setDollar(value)} dollar = {dollar} convertUSD = {convertUSD}/>
      </div>
    </div>
  )
} 

export default App;
*/